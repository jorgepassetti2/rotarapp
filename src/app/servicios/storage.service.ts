import { Injectable } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';

@Injectable({
  providedIn: 'root'
})
export class StorageService {


  constructor(
    private _storage:AngularFireStorage
  ) { }


/////SETTERS/////

async uploadFile(file, path){
  const fileRef = this._storage.ref(path);
  var task = await this._storage.upload(path, file);
    return await new Promise(resolve=>{
      fileRef.getDownloadURL().subscribe(res=>{
        console.log(res);
        resolve(res);
      });
    });     

}
}
