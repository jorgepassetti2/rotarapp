import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Publicacion } from '../classes/publicacion';

@Injectable({
  providedIn: 'root'
})
export class PublicacionesService {

  constructor(
    private db: AngularFirestore,

  ) { }

  //getters
  getPublicaciones(){
    return this.db.collection("publicaciones").valueChanges();
  }

  //setters
  setPublicacion(publicacion:Publicacion){
    return this.db.collection("publicaciones").doc(publicacion.id).set(Object.assign({}, publicacion));
  }

}
