import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';

import { AuthService } from '../../servicios/auth.service';

@Component({
  selector: 'app-iniciar-sesion',
  templateUrl: './iniciar-sesion.page.html',
  styleUrls: ['./iniciar-sesion.page.scss'],
})
export class IniciarSesionPage implements OnInit {

  email: string;
  password: string;

  constructor(private modalCtrl: ModalController,
  private authService: AuthService,
  public router: Router  ) { }

  ngOnInit() {
  }

  onSubmitLogin() {
    
    this.authService.login(this.email, this.password).then( res => {
      this.modalCtrl.dismiss();
      this.router.navigate(['/inicio']);
    }).catch(err => { alert('El usuario ingresado no existe')});
  }

  cargarImagen() {
  	console.log("Cargando Imagen...");
  }

  crearPulicacion() {
  	this.modalCtrl.dismiss()
  }

  regresar() {
  	this.modalCtrl.dismiss()
  }

}