import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Publicacion } from 'src/app/classes/publicacion';
import { PublicacionesService } from 'src/app/servicios/publicaciones.service';
import { CrearPublicacionPage } from '../crear-publicacion/crear-publicacion.page';
import { DetallePubComponent } from './detalle-pub/detalle-pub.component';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

  public publicaciones:Publicacion[]= []

  constructor(
    private modalCtrl: ModalController,
    private pubService:PublicacionesService
    ) { }

  ngOnInit() {
    this.getPublicaciones();
  }

  //Esta funcion es para obtener las publicaciones
  getPublicaciones(){
    this.pubService.getPublicaciones().subscribe((publicaciones:Publicacion[])=>{
      this.publicaciones=publicaciones;
    })
  }

  //Esta funcion es para abrir el detalle de la publicacion
  async abrirDetalle(p:Publicacion){
    const modal = await this.modalCtrl.create({
      component: DetallePubComponent,
      componentProps: {
        publicacion: p,
      },
    });
    await modal.present();
  }

  // para filtrar publicaciones
  filtrarPublicaciones(){
    
  }

  // Abrir Modal para crear una publicacion
  async abrirModalPublicaciones() {
    console.log("Abriendo Modal")
    
    const modal = await this.modalCtrl.create({
      component: CrearPublicacionPage,
      componentProps: {
        prop1: "test",
        prop2: "test2"
      },
    });

    await modal.present();

  }
}
