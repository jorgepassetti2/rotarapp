import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { Publicacion } from 'src/app/classes/publicacion';

@Component({
  selector: 'app-detalle-pub',
  templateUrl: './detalle-pub.component.html',
  styleUrls: ['./detalle-pub.component.scss'],
})
export class DetallePubComponent implements OnInit {

  publicacion= new Publicacion;
  constructor(
    private navParams:NavParams,
    private modalController: ModalController
  ) { }

  ngOnInit() {
    this.publicacion=this.navParams.get("publicacion");
  }

  //para obtener los datos del usuario que creo esta publicacion
  getUser(){

  }

  back(){
    this.modalController.dismiss();
  }
}
