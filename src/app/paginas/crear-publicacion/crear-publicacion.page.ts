import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Publicacion } from 'src/app/classes/publicacion';
import { PublicacionesService } from 'src/app/servicios/publicaciones.service';
import { StorageService } from 'src/app/servicios/storage.service';

@Component({
  selector: 'app-crear-publicacion',
  templateUrl: './crear-publicacion.page.html',
  styleUrls: ['./crear-publicacion.page.scss'],
})
export class CrearPublicacionPage implements OnInit {
  publicacion= new Publicacion;
  constructor(
    private modalCtrl: ModalController,
    private pubServ:PublicacionesService,
    private storageService:StorageService
    ) { }

  ngOnInit() {
  }

  fileChangeEvent(ev){
    console.log(ev);
    const file = ev.target.files[0]; 
    this.storageService.uploadFile(file, "publicaciones/"+file.name).then((url:string)=>{
      this.publicacion.imagen=url;
    });
  }


  cargarImagen() {
  	console.log("Cargando Imagen...");
  }
  ver(ev){
    console.log(ev);
    
  }

  crearPublicacion() {
    if(this.publicacion.nombre==""){
      alert("Debe ingresar un nombre");
      return false;
    }
    if(this.publicacion.descripcion==""){
      alert("Debe ingresar una descripcion");
      return false;
    }
    if(this.publicacion.tipo==""){
      alert("Debe ingresar un tipo");
      return false;
    }
    this.publicacion.id=Date.now().toString();
    console.log(this.publicacion);
    
    this.pubServ.setPublicacion(this.publicacion).then(res=>{
      this.publicacion= new Publicacion;
      this.modalCtrl.dismiss();
    }).catch(err=>{
      console.log(err);
    });
  	
  }

  regresar() {
  	this.modalCtrl.dismiss()
  }

}
