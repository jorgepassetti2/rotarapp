import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';


import { ModalController } from '@ionic/angular';
import { IniciarSesionPage } from './paginas/iniciar-sesion/iniciar-sesion.page';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public appPages1 = [
    {
      title: 'Inicio',
      url: '/inicio',
      icon: 'home'
    },
    {
      title: 'Consultas',
      url: '/consultas',
      icon: 'paper-plane'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private modalCtrl: ModalController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    const path = window.location.pathname.split('folder/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages1.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
      }
    
  }
  async abrirModalIniciarSesion() {
      console.log("Abriendo Modal")
      
      const modal = await this.modalCtrl.create({
        component: IniciarSesionPage,
        componentProps: {
          prop1: "test",
          prop2: "test2"
        },
      });

      await modal.present();
    
  }

}
