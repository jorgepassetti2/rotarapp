// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const firebaseConfig = {
    apiKey: "AIzaSyBUwpTw4iUbkV2HaMPyG9V1uiWGtgrjv9E",
    authDomain: "cuchitapp.firebaseapp.com",
    databaseURL: "https://cuchitapp.firebaseio.com",
    projectId: "cuchitapp",
    storageBucket: "cuchitapp.appspot.com",
    messagingSenderId: "986283476262",
    appId: "1:986283476262:web:7d4e93c1d561034e362de7",
    measurementId: "G-RFNSBDZNV2"
  };
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
